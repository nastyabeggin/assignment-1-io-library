;lib.inc
%define space 0x20
%define tab 0x9
%define new_line 0xA

section .data 
newline_char: db 10


section .text 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60			; invoke 'exit' system call  
    syscall 


; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
        xor rax, rax		; make sure rax is zeroed
    .loop:			; main loop
        cmp byte [rdi + rax], 0 ; check if the current symbol is null-terminator
	je .end			; jump if it is
	inc rax			; otherwise increment length counter
	jmp .loop		; start the .loop again
    .end:
        ret			; return length in rax 


	
; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    mov rsi, rdi       		; copy destination address to source
    push rsi
    call string_length      ; count length 
    pop rsi
    mov rdx, rax        	; save string to data register
    mov rdi, 1          	; to stdout 
    mov rax, 1          	; store system call number in rax
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax
    push rdi
    mov rsi, rsp            ; set pointer to stack
    pop rdi                 ; clear stack 
    mov rdi, 1          	; to stdout
    mov rax, 1              ; store system call number in rax
    mov rdx, 1          	; char length = 1 
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, newline_char
    jmp print_char    

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
    mov rax, rdi            ; number to rax
    mov r8, rsp             ; save initial stack pointer
    mov r9, 10              ; used for div
    push 0                  ; end of line to stack
    .loop: 
        mov rdx, 0          ; zeroing data register 
        div r9              ; whole part to rax, remained to rdx
        add rdx, '0'         ; convert to ASCII
        dec rsp             ; decrease stack pointer
        mov byte[rsp], dl   ; digit to stack
        cmp rax, 0          ; compare rax to zero 
        ja .loop            ; check if equals
        mov rdi, rsp        ; save stack pointer 
        push r8             ; save initial stack pointer
        call print_string   ; output int 
        pop r8              
        mov rsp, r8         ; set initital stack pointer
        ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
    cmp rdi, 0              ; compare with zero
    jge print_uint          ; call print_uint if >= 0 
    mov r8, rdi             ; else save char
    mov rdi, '-'       
    call print_char         ; print minus
    mov rdi, r8             ; restore number
    neg rdi                 ; change sign 
    jmp print_uint         ; print number      

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    mov r8, 0
    mov r9, 0
    mov rcx, 0
    .loop:
        mov r8b, byte[rdi+rcx] ; take symbol from first line
        mov r9b, byte[rsi+rcx] ; take symbol from first line
        cmp r8b, r9b           ; compare them 
        jne .false             ; if not equals go to .false
        cmp r8b,0              ; check if its end of the string
        je .true               ; if it is go to .true
        inc rcx                ; increase counter
        jmp .loop
    .true:
        mov rax, 1              ; return 1
        ret
    .false:
        mov rax, 0              ; return 0
        ret
    

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    dec rsp			; decrease stack pointer
    mov rsi, rsp		; set addres
    mov rdx, 1			; length of char
    syscall
    test rax, rax               
    jz .END                     ; go to .eof if end of the line 
    mov al, [rsp]               ; save char
    .END:
    inc rsp			; allocate space in stack
    ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rax, rax 
    mov rcx, 0
    mov r8, rsi     ; buffer length
    mov r10, 0      ; length of string
    .loop:
        push rcx    ; save variables in stack
        push rsi
        push rdi
        call read_char  ; read one symbol
        pop rdi
        pop rsi
        pop rcx
        cmp rax, space   ; check if its \t, \n or space
        je .skip
        cmp rax, tab
        je .skip
        cmp rax, new_line
        je .skip
        cmp rax, 0      ; check if its new line
        je .end
        mov [rdi + rcx], rax ;  save char to buffer
        inc rcx             
        inc r10              ;  increase length 
        cmp rcx, r8          ; check if we not exceeded buffer size
	    jge .buffer_limit    ; if buffer limit is reached go to .buffer_limity
        jmp .loop            ; repeat!
        .skip:
            cmp rcx, 0       ; check if it's beginning 
            je .loop         ; if yes – read symbol 
        .end:
            xor rax, rax
            mov [rdi + rcx], rax ; save zero
            mov rax, rdi         ; save buffer address
            mov rdx, r10         ; save length 
            ret
        .buffer_limit:
            xor rax, rax         ; zeroing
            ret
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx  ; stores length
    .loop:
        xor r9, r9
        mov r9b, byte[rdi + rcx]     ; get the symbol into lower 8 bits 
        cmp r9b, '0'                ; check if its code below 0
        jb .end
        cmp r9b, '9'                ; check if its code above 9  
        ja .end
        inc rcx                     ; length counter
        sub r9b, '0'                ; remove zeroes 
        imul rax, 10                ; multiply rax
        add rax, r9                 ; rax + num 
        jmp .loop
    .end:
        mov rdx, rcx
        ret



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    cmp byte[rdi], '-'  ; check if the number is negative
    je .negative
    jmp parse_uint      ; if its not negative – consider it as uint 

    .negative:
        inc rdi         ; point to digit (not minus)
        call parse_uint ; work with positive part
        neg rax         ; return to negative
        inc rdx         ; length + 1
        ret 


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    xor r8, r8          ; stores length
    push rdi            ; store the registers that can be changed
    push rsi
    push rdx 
    call string_length  ; get string length
    pop rdx
    pop rsi
    pop rdi 
    mov r8, rax         ; save length
    cmp rax, rdx        ; compare buffer size and string length
    jbe .correct_length ; if it's less or equal go to .correct_length
    xor rax, rax        ; return length
	ret
    .correct_length:
        xor rcx, rcx    ; clear rcx
        .loop: 
            cmp rcx, r8 ; check if its end of the line
            jg .end     
            mov r9, [rdi+rcx] ; move symbol to buffer
            mov [rsi+rcx], r9 
            inc rcx
            jmp .loop
    .end:
        mov rax, r8     ; return string length
        ret

